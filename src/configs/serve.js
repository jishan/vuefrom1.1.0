export default {
  apiUrl: '/jd',
  title: '简搭云科技',
  logo: '/logo.png',
  author: '简搭云科技通用管理系统快速开发框架',
  isdevtool: false,
  versiontype: 'form',
  keycode:
    'Aq1UtHWTr1/ewrWt/SydI1JcUWosyNypSb1S8QwheZPIrrP2Zo6vJ6rkYyEJhxzSH6L/aXHH7UKgGPfgjBlfYHRbVVnsgfTrxf1bEn2JrdOa8APf1/VsHCYfjOJFEKMhFP7QDA3mb4NxHJ61+hvfI821rd0QQi9Bz/XXJyjpzSM=',
};
